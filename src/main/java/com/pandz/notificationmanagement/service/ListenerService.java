package com.pandz.notificationmanagement.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pandz.notificationmanagement.constant.Constant;
import com.pandz.notificationmanagement.model.EmailModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class ListenerService {
  private final static Logger log = LoggerFactory.getLogger(ListenerService.class);

  private final ObjectMapper mapper;

  @Autowired
  private JavaMailSender emailSender;

  @Autowired
  public ListenerService(ObjectMapper mapper) { this.mapper = mapper; }

  public void sendSimpleMessage(String to, String subject, String text) {
    log.info("Start sending email message!");
    SimpleMailMessage message = new SimpleMailMessage();
    message.setFrom("topanardianto@gmail.com");
    message.setTo(to);
    message.setSubject(subject);
    message.setText(text);
    emailSender.send(message);
  }

  @RabbitListener(queues = Constant.NOTIF_PAYMENT)
  public void listenEmail(String message) {
    log.info("Start listening message");
    try {
      EmailModel emailModel = mapper.readValue(message, EmailModel.class);

      log.info("Converting to model....");
      log.info("Message is: " + emailModel.toString());

      // send email
      sendSimpleMessage(emailModel.getTo(), emailModel.getSubject(), emailModel.getMessage());
      log.info("Success send email");

    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
  }

  public static void main(String[] args) {
  }
}
